Intelligence
Can't remember what toilet is
Somehow knows what toilet is
Will die if left alone
The bare minimum for a pet yu
Can perfectly count up to 1
If alone it will probably live
Won't be an embarassing pet
Might fail the gold badge test
Can do simple errands
Meets the standard of other yukkuri
Is not really like other yukkuri...

Affection
Cautious...
Hardly attached to you
Starting to warm up to you
Approaches positively
Puts a little faith in you
Feels easy around you
Considerable trust
Quite the trust
A strong sense of love
Would listen to unreasonable demands
Totally takes it easy

Scumminess
Pure and innocent
Can take it easy
A bit egotistical
Acts egotistically
Only thinks for herself
If she's good, all's fine
Looks down on other yukkuri
Other yukkuri exist for her sake
She's never wrong
Thinks she's a blessing
Thinks the world is hers

Hunger
Starving
Hungry
Getting hungry
Not really hungry
Full

Fear
Not scared of humans
Cautious around humans
Thinks humans are dangerous
Scared of humans
Very scared of humans
Extremely scared of humans

Reflexes
Can barely move
Slow
So-so
A bit quick
Somewhat quick
Quite agile
Considerably agile
Athletic
Really athletic
Olympic skills
Is it really a yukkuri?

Stamina
Forever easy (slow)
Feeble
Weak
Normal
Healthy
Very healthy
Outstandingly healthy

Disease
Healthy
Constipated
Hemorrhoids
Castrated
Mold

Relationships
Hates
Indifferent
Awkward
Close
Love
Absolute Love

Pickiness (food)
No sense of taste
Eats almost anything
Unsophisticated tastes
Not quite the palate
So-so sense of taste
A bit spoiled
Spoiled
Luxurious palate
Only sweet-sweets and above
Sweet-sweets aren't enough
Only excellent gourmet food





Species
Marisa
Shell Marisa
Reimu
Wasa Reimu

